from pathlib import Path

import numpy as np
import pandas as pd

import bootstrap
import calc
import constants
import files
import frames
import newcols
import plots
import settings

pd.set_option('display.width', 2000)
pd.set_option('display.max_columns', 14)
pd.set_option('display.max_rows', 30)


class Scop(settings.Settings, newcols.NewCols, constants.Constants):
    """
    Calculate scaling factors by comparing parallel measurements
    """

    def __init__(self):
        self.df = pd.DataFrame()
        self.scaling_factors_df = pd.DataFrame()
        self.outfile = None

    def apply_sf(self):
        """Apply scaling factors from file, correct fluxes"""
        self.outdir = Path(self.outdir) / "2-Application"
        self.outfile = self.outdir / "corrected_fluxes.csv"
        self.readfile(file=self.cr_file)
        # self.readfile(file=self.cr_file, nrows=10000)
        self.init_newcols()
        self.detect_daytime(swin=self.df[self.cr_swin_col])
        self.aerodynamic_resistance(u=self.df[self.cr_u_col], ustar=self.df[self.cr_ustar_col], rem_outliers=True)
        self.dry_air_density(rho_v=self.df[self.cr_rho_v_col], rho_a=self.df[self.cr_rho_a_col])
        self.instrument_surface_temperature(ta=self.df[self.cr_ta_col])
        self.unscaled_flux_correction_term(ts=self.df[self.ts_col],
                                           ta=self.df[self.cr_ta_col],
                                           qc_umol=self.df[self.cr_qc_mmol_col].multiply(1000),
                                           ra=self.df[self.ra_col],
                                           rho_v=self.df[self.cr_rho_v_col],
                                           rho_d=self.df[self.rho_d_col],
                                           lut_gapfill=True,
                                           rem_outliers=True)
        self.assign_scaling_factors(file=self.cr_sf_file, classvar_col=self.cr_class_var_col, lut_gapfill=True)
        self.calc_corrected_fluxes(op_co2_flux_nocorr_col=self.cr_op_co2_flux_nocorr_col)
        self.plot_cr()
        self.savefile()

    def calc_sf(self):
        """Calculate scaling factors from parallel measurements"""
        self.outdir = Path(self.outdir) / "1-Calculation"
        self.outfile = self.outdir / "data.csv"
        self.readfile(file=self.pm_file)
        self.restrict_flux_quality(flag_col=self.pm_qcf_op_co2_flux_nocorr_col, flux_col=self.pm_op_co2_flux_nocorr_col)
        # self.custom_class_var()
        self.init_newcols()
        self.detect_daytime(swin=self.df[self.pm_swin_col])
        self.aerodynamic_resistance(u=self.df[self.pm_u_col], ustar=self.df[self.pm_ustar_col], rem_outliers=True)
        self.dry_air_density(rho_v=self.df[self.pm_rho_v_col], rho_a=self.df[self.pm_rho_a_col])
        self.instrument_surface_temperature(ta=self.df[self.pm_ta_col])
        self.unscaled_flux_correction_term(ts=self.df[self.ts_col],
                                           ta=self.df[self.pm_ta_col],
                                           qc_umol=self.df[self.pm_qc_mmol_col].multiply(1000),
                                           ra=self.df[self.ra_col],
                                           rho_v=self.df[self.pm_rho_v_col],
                                           rho_d=self.df[self.rho_d_col],
                                           rem_outliers=True,
                                           lut_gapfill=True)
        self.scaling_factors_df = self.optimize_scaling_factors(class_var_col=self.pm_class_var_col)
        self.assign_scaling_factors(classvar_col=self.pm_class_var_col, lut_gapfill=True)
        self.calc_corrected_fluxes(op_co2_flux_nocorr_col=self.pm_op_co2_flux_nocorr_col)
        self.stats()
        self.plot_pm()
        self.savefile()

    def restrict_flux_quality(self, flag_col, flux_col):
        """Keep only fluxes of a certain quality"""
        _filter_qcf = self.df[flag_col] == 0
        self.df[flux_col] = self.df.loc[_filter_qcf, flux_col]

    def readfile(self, file, nrows=None):
        """Read file to dataframe

        :param file: str
        :return:
        """
        self.df = files.read(src=file, nrows=nrows)

    # def custom_class_var(self):
    #     """Select custom variable that is then used to divide data into x classes
    #
    #     This method enables the calculation of scaling factors in dependence of the
    #     time stamp, e.g. scaling factors for each month.
    #     """
    #     if self.class_var_col == 'custom':
    #         self.df[('_custom', '--')] = self.df.index.month  #
    #         self.num_classes = None
    #         self.class_var_col = ('_custom', '--')

    def detect_daytime(self, swin):
        # Daytime, nighttime
        self.df[self.daytime_col], self.daytime_filter, self.nighttime_filter = \
            frames.detect_daytime(swin=swin)

    def stats(self):
        cols = [self.pm_op_co2_flux_nocorr_col,
                self.pm_cp_co2_flux_col]

        cols.append(self.op_co2_flux_corr_col)

        _stats_df = self.df[cols].copy()
        _stats_df = _stats_df.dropna()
        _numvals = len(_stats_df)

        print("\nCUMULATIVE FLUXES:")
        print(f"Values: {_numvals}")
        _cumsum_opnocorr = _stats_df[self.pm_op_co2_flux_nocorr_col].sum()
        _cumsum_cptrueflux = _stats_df[self.pm_cp_co2_flux_col].sum()
        _perc = (_cumsum_opnocorr / _cumsum_cptrueflux) * 100
        print(f"OPEN-PATH (uncorrected): {_cumsum_opnocorr:.0f}  ({_perc:.1f}% of true flux)")
        print(f"ENCLOSED-PATH (true flux): {_cumsum_cptrueflux:.0f}")
        _cumsum = _stats_df[self.op_co2_flux_corr_col].sum()
        _perc = (_cumsum / _cumsum_cptrueflux) * 100
        print(f"OPEN-PATH (corrected): {_cumsum:.0f}  ({_perc:.1f}% of true flux)")
        print("\n\n")

    def savefile(self):
        self.df.to_csv(self.outfile)

    def init_newcols(self):
        self.df[self.daytime_col] = np.nan
        self.df[self.ra_col] = np.nan
        self.df[self.rho_d_col] = np.nan
        self.df[self.ts_col] = np.nan
        self.df[self.fct_unsc_col] = np.nan
        self.df[self.fct_unsc_gf_col] = np.nan
        self.df[self.fct_unsc_lutvals_col] = np.nan
        self.df[self.class_var_group_col] = np.nan
        self.df[self.sf_col] = np.nan
        self.df[self.sf_lutvals_col] = np.nan
        self.df[self.class_var_group_col] = np.nan
        self.df[self.fct_col] = np.nan
        self.df[self.op_co2_flux_corr_col] = np.nan

    def plot_cr(self):
        """Plot results from applying scaling factors from file to data that needs correction"""
        plots.SeriesVars(plot_df=self.df.copy(), outdir=self.outdir)
        plots.SeriesFlux(outdir=self.outdir,
                         daytime=self.df[self.daytime_col],
                         uncorrected_flux=self.df[self.cr_op_co2_flux_nocorr_col],
                         corrected_flux=self.df[self.op_co2_flux_corr_col])
        plots.DielCyclesVars(plot_df=self.df.copy(), outdir=self.outdir)
        plots.DielCyclesFlux(df=self.df.copy(), outdir=self.outdir,
                             uncorrected_flux_col=self.cr_op_co2_flux_nocorr_col,
                             corrected_flux_col=self.op_co2_flux_corr_col)
        plots.CumulativeFlux(df=self.df.copy(), outdir=self.outdir,
                             daytime_col=self.daytime_col,
                             uncorrected_flux_col=self.cr_op_co2_flux_nocorr_col,
                             corrected_flux_col=self.op_co2_flux_corr_col)

    def plot_pm(self):
        """Plot results from scaling factor calculations from parallel measurements"""
        # pass
        plots.SeriesVars(plot_df=self.df.copy(), outdir=self.outdir)
        plots.SeriesFlux(outdir=self.outdir,
                         daytime=self.df[self.daytime_col],
                         uncorrected_flux=self.df[self.pm_op_co2_flux_nocorr_col],
                         true_flux=self.df[self.pm_cp_co2_flux_col],
                         corrected_flux=self.df[self.op_co2_flux_corr_col])
        plots.DielCyclesVars(plot_df=self.df.copy(), outdir=self.outdir)
        plots.DielCyclesFlux(df=self.df.copy(), outdir=self.outdir,
                             uncorrected_flux_col=self.pm_op_co2_flux_nocorr_col,
                             corrected_flux_col=self.op_co2_flux_corr_col,
                             true_flux_col=self.pm_cp_co2_flux_col)
        plots.CumulativeFlux(df=self.df.copy(), outdir=self.outdir,
                             daytime_col=self.daytime_col,
                             uncorrected_flux_col=self.pm_op_co2_flux_nocorr_col,
                             corrected_flux_col=self.op_co2_flux_corr_col,
                             true_flux_col=self.pm_cp_co2_flux_col)

    def calc_corrected_fluxes(self, op_co2_flux_nocorr_col):
        # Use only a fraction of the unscaled flux correction term
        # Add scaled correction flux to original WPL-only open-path flux
        self.df[self.op_co2_flux_corr_col], self.df[self.fct_col] = \
            calc.corrected_flux(uncorrected_flux=self.df[op_co2_flux_nocorr_col],
                                fct_unsc_gf=self.df[self.fct_unsc_gf_col],
                                sf_gf=self.df[self.sf_gf_col])

    def assign_scaling_factors(self, classvar_col: tuple, file: str = None, lut_gapfill: bool = False):
        # TODO Since the scaling factors are assigned using the LUT, there are probably some flux values where the class variable (ustar) is outside the lookup range the LUT can provide.
        # TODO For these fluxes, the last known scaling factor is used.
        if file:
            file = Path(file)
            self.scaling_factors_df = pd.read_csv(file)

        for ix, row in self.scaling_factors_df.iterrows():
            _filter_group = row
            _filter_group = (self.df[self.daytime_col] == row['DAYTIME']) \
                            & (self.df[classvar_col] >= row['GROUP_CLASSVAR_MIN']) \
                            & (self.df[classvar_col] <= row['GROUP_CLASSVAR_MAX'])
            self.df.loc[_filter_group, self.class_var_group_col] = row['GROUP_CLASSVAR']
            self.df.loc[_filter_group, self.sf_col] = row['SF_MEDIAN']

        if lut_gapfill:
            self.df[self.sf_gf_col], self.df[self.sf_lutvals_col] = \
                calc.gapfilling_lut(self.df[self.sf_col])

    def optimize_scaling_factors(self, class_var_col):
        optimize = bootstrap.OptimizeScalingFactors(df=self.df,
                                                    daytime_col=self.daytime_col,
                                                    class_var_col=class_var_col,
                                                    class_var_group_col=self.class_var_group_col,
                                                    num_classes=self.pm_num_classes,
                                                    outdir=self.outdir)
        return optimize.get()

    def aerodynamic_resistance(self, u, ustar, rem_outliers: bool = False):
        # Aerodynamic resistance
        self.df[self.ra_col] = \
            calc.aerodynamic_resistance(u_ms=u,
                                        ustar_ms=ustar)
        if rem_outliers:
            self.df[self.ra_col] = \
                calc.remove_outliers(series=self.df[self.ra_col], plot_title="X", n_sigmas=20)

    def dry_air_density(self, rho_v, rho_a):
        # Dry air density
        self.df[self.rho_d_col] = \
            calc.dry_air_density(rho_v=rho_v,
                                 rho_a=rho_a,
                                 M_AIR=self.M_AIR,
                                 M_H2O=self.M_H2O)

    def instrument_surface_temperature(self, ta):
        self.df[self.ts_col] = \
            calc.surface_temp_jar09(ta=ta,
                                    daytime_filter=self.daytime_filter,
                                    nighttime_filter=self.nighttime_filter)

    def unscaled_flux_correction_term(self, ts, ta, qc_umol, ra, rho_v, rho_d,
                                      lut_gapfill: bool = False, rem_outliers: bool = False):
        print("Calculating unscaled flux correction term ...")

        self.df[self.fct_unsc_col] = \
            calc.flux_correction_term_unscaled(ts=ts, ta=ta, qc_umol=qc_umol, ra=ra, rho_v=rho_v, rho_d=rho_d)

        # Remove outliers
        if rem_outliers:
            self.df[self.fct_unsc_col] = \
                calc.remove_outliers(series=self.df[self.fct_unsc_col],
                                     plot_title="XXX")

        # Gap-filling (LUT)
        if lut_gapfill:
            self.df[self.fct_unsc_gf_col], self.df[self.fct_unsc_lutvals_col] = \
                calc.gapfilling_lut(series=self.df[self.fct_unsc_col])


def main():
    # calculate_scaling_factors = Scop().calc_sf()
    apply_scaling_factors = Scop().apply_sf()
    print("End.")


if __name__ == '__main__':
    main()
