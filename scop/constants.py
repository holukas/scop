class Constants:
    M_AIR = 0.028965  # molar mass of dry air (kg mol-1) (M_AIR)
    M_H2O = 0.018  # molar mass of water vapor (kg mol-1) (M_H2O)
    T0_K = 273.15  # Absolute zero (K)
