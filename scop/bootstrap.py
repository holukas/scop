from pathlib import Path

import numpy as np
import pandas as pd
from scipy.optimize import minimize_scalar

import frames
import newcols
import plots
import settings


class OptimizeScalingFactors(settings.Settings, newcols.NewCols):

    def __init__(self, df, daytime_col, class_var_col, class_var_group_col, num_classes, outdir):
        self.df = df
        self.daytime_col = daytime_col
        self.class_var_col = class_var_col
        self.class_var_group_col = class_var_group_col
        self.num_classes = num_classes

        # If data are not bootstrapped, set flag to False
        if self.pm_num_bootstrap_runs == 0:
            self.bootstrapped = False
            self.pm_num_bootstrap_runs = 9999  # Set to arbitrary number
        else:
            self.bootstrapped = True

        if self.class_var_col[0] == '_custom':
            num_classes = len(self.df[self.class_var_col].unique())
        self.scaling_factors_df = frames.init_scaling_factors_df(num_classes=num_classes)

        self.outfile_csv = outdir / "self-heating_scaling_factors.csv"
        self.outfile_plot = outdir / "self-heating_scaling_factors_per_class.png"

        self.run()

    def run(self):
        self.bootstrapping()
        self.plot()
        self.print_stats()
        self.savefile()

    def plot(self):
        plots.ScalingFactors(plot_df=self.scaling_factors_df,
                             outplot=self.outfile_plot,
                             classvar_col=self.class_var_col)

    def savefile(self):
        print(f"--> Saving scaling factors to file: {self.outfile_csv}")
        self.scaling_factors_df.to_csv(self.outfile_csv)

    def print_stats(self):
        print("BOOTSTRAPPING RESULTS")
        print(f"class variable: {self.class_var_col}")
        print(f"number of classes: {self.num_classes}")

        print("\nSF results:")
        print("====================")
        print(f"scaling factor (daytime median): {self.scaling_factors_df.loc[1, 'SF_MEDIAN'].median():.3f}")
        print(
            f"scaling factor (nighttime median): {self.scaling_factors_df.loc[0, 'SF_MEDIAN'].median():.3f}")
        print(f"scaling factor (overall median): {self.scaling_factors_df.loc[:, 'SF_MEDIAN'].median():.3f}")
        print(
            f"sum of squares (daytime median): {self.scaling_factors_df.loc[1, 'SOS_MEDIAN'].median():.3f}")
        print(
            f"sum of squares (nighttime median): {self.scaling_factors_df.loc[0, 'SOS_MEDIAN'].median():.3f}")
        print(
            f"sum of squares (overall median): {self.scaling_factors_df.loc[:, 'SOS_MEDIAN'].median():.3f}")

    def get(self):
        return self.scaling_factors_df

    def bootstrapping(self):

        # Group data records by daytime / nighttime membership
        _grouped_by_daynighttime = self.df.groupby(self.daytime_col)

        # Loop through daytime / nighttime data
        for _group_daynighttime, _group_daynighttime_df in _grouped_by_daynighttime:

            if self.class_var_col[0] == '_custom':
                _group_daynighttime_df[self.class_var_group_col] = \
                    _group_daynighttime_df[self.class_var_col]
            else:
                # Divide data into x class variable groups w/ same number of values
                _group_daynighttime_df[self.class_var_group_col] = \
                    pd.qcut(_group_daynighttime_df[self.class_var_col],
                            q=self.num_classes, labels=False)

            # Group data records by class variable membership
            _grouped_by_class_var = _group_daynighttime_df.groupby(self.class_var_group_col)

            # Loop through class variable groups
            for _group_class_var, _group_class_var_df in _grouped_by_class_var:
                # Bootstrap group data

                bts_factors = []
                bts_sum_of_squares = []
                bts_num_vals = []

                for bts_run in range(0, self.pm_num_bootstrap_runs):
                    num_rows = int(len(_group_class_var_df.index))

                    if self.bootstrapped:
                        # Use bootstrapped data
                        bts_sample_df = _group_class_var_df.sample(n=num_rows,
                                                                   replace=True)  # Take sample w/ replacement
                    else:
                        # Use measured data in cas
                        bts_sample_df = _group_class_var_df

                    bts_sample_df.sort_index(inplace=True)

                    result = self.optimize_factor(target=bts_sample_df[self.pm_op_co2_flux_nocorr_col],
                                                  reference=bts_sample_df[self.pm_cp_co2_flux_col],
                                                  fct_unsc_gf=bts_sample_df[self.fct_unsc_gf_col])
                    bts_factors.append(result.x)  # x = scaling factor
                    bts_sum_of_squares.append(result.fun)
                    bts_num_vals.append(bts_sample_df[self.class_var_col].count())

                    # Break if only working with measured data (no bootstrapping)
                    if not self.bootstrapped:
                        break

                print(f"Finished {self.pm_num_bootstrap_runs} bootstrap runs for group {_group_class_var} "
                      f"in daytime = {_group_daynighttime}")

                # Stats, aggregates for current class group
                location = tuple([_group_daynighttime, _group_class_var])
                self.scaling_factors_df.loc[location, f'DAYTIME'] = _group_daynighttime
                self.scaling_factors_df.loc[location, f'GROUP_CLASSVAR'] = _group_class_var
                self.scaling_factors_df.loc[location, f'GROUP_CLASSVAR_MIN'] = _group_class_var_df[
                    self.class_var_col].min()
                self.scaling_factors_df.loc[location, f'GROUP_CLASSVAR_MAX'] = _group_class_var_df[
                    self.class_var_col].max()
                self.scaling_factors_df.loc[location, f'BOOTSTRAP_RUNS'] = self.pm_num_bootstrap_runs

                self.scaling_factors_df.loc[location, f'SF_MEDIAN'] = np.median(bts_factors)
                self.scaling_factors_df.loc[location, f'SOS_MEDIAN'] = np.median(
                    bts_sum_of_squares)
                self.scaling_factors_df.loc[location, f'NUMVALS_AVG'] = np.mean(bts_num_vals)
                self.scaling_factors_df.loc[location, f'SF_Q25'] = np.quantile(bts_factors, 0.25)
                self.scaling_factors_df.loc[location, f'SF_Q75'] = np.quantile(bts_factors, 0.75)
                self.scaling_factors_df.loc[location, f'SF_Q01'] = np.quantile(bts_factors, 0.01)
                self.scaling_factors_df.loc[location, f'SF_Q99'] = np.quantile(bts_factors, 0.99)

    def optimize_factor(self, target, reference, fct_unsc_gf):
        """Optimize factor by minimizing sum of squares b/w corrected target and reference"""
        optimization_df = pd.DataFrame()
        optimization_df['target'] = target
        optimization_df['reference'] = reference
        optimization_df['fct_unscaled_col'] = fct_unsc_gf
        optimization_df = optimization_df.dropna()

        target = optimization_df['target'].copy()
        reference = optimization_df['reference'].copy()
        fct_unsc_gf = optimization_df['fct_unscaled_col'].copy()  # Unscaled flux correction term

        result = minimize_scalar(self.calc_sumofsquares, args=(fct_unsc_gf, target, reference),
                                 method='Bounded', bounds=[-1, 4])
        # result = minimize_scalar(self.calc_sumofsquares, args=(fct_unscaled, target, reference),
        #                          method='Golden', tol=0)
        # print("=" * 40)
        return result

    def calc_sumofsquares(self, factor, unsc_flux_corr_term, target, reference):
        corrected = target + unsc_flux_corr_term.multiply(factor)

        diff2 = np.sqrt((reference - corrected) ** 2)
        sum_of_squares = diff2.sum()

        # diff2 = (reference - corrected)
        # # diff2 = diff2.abs()
        # sum_of_squares = diff2.sum()

        # diff2=(corrected.corr(reference))**2
        # sum_of_squares = 1 - diff2

        # diff2 = corrected.sum() - reference.sum()
        # sum_of_squares = np.abs(diff2)

        # print(f"factor: {factor}  sos: {sum_of_squares}")
        return sum_of_squares
